# Toolbox

It's not always easy to be a software developer. This file contains a link collection about things I have found somewhat useful.


## Table of contents
- [Awesome lists](#awesome-lists)
- [Data & Datasets](#data)
- [Tools and Services](#tools-and-services)
- [Docker](#docker)
- [API management](#api-management)
- [Libraries](#libraries)
- [Quality](#quality)
- [Misc](#misc)


## Awesome lists
- [Curated list of awesome lists](https://github.com/sindresorhus/awesome)
- [An awesome list of hosted tools for web development](https://github.com/lvwzhen/tools)
- [Awesome list of multipart computing](https://github.com/rdragos/awesome-mpc)
- [A curated list of awesome Machine Learning frameworks, libraries and software](https://github.com/josephmisiti/awesome-machine-learning)
- [A curated list of awesome Deep Learning tutorials, projects and communities](https://github.com/ChristosChristofidis/awesome-deep-learning)
- [Curated list of projects that build non-financial applications of blockchain](https://github.com/machinomy/awesome-non-financial-blockchain)



## Data

### Datasets
- [A curated list of awesome JSON datasets that don't require authentication](https://github.com/jdorfman/awesome-json-datasets)
- [A topic-centric list of high-quality open datasets in public domains. By everyone, for everyone!](https://github.com/awesomedata/awesome-public-datasets)


## Tools and Services


### Services
- [Okta Developer - Authentication](https://developer.okta.com/)
- [mLab - Database-as-a-Service for MongoDB](https://mlab.com/)
- [Kumulos - The Mobile App Performance Management (mAPM) Platform](https://www.kumulos.com/)
- [httpbin - HTTP Request & Response Service](http://httpbin.org/)


### Swagger - API design

"Swagger is the world�s largest framework of API developer tools for the OpenAPI Specification(OAS)"

- [Swagger.io](https://swagger.io/)
- [Swagger Hub](https://swaggerhub.com/)
- [https://github.com/Rebilly/ReDoc](ReDoc)


### Generators
- [HUGO - Open-source static website generator](http://gohugo.io/)



### JSON
- [JSON Editor Online - online tool to view, edit, and format JSON](https://jsoneditoronline.org/)
- [JSON Schema Faker  - Generate fake data that conform to the schema](http://json-schema-faker.js.org/)
- [JSONSchema.Net - generate JSON schema from JSON](https://jsonschema.net)
- [JSON Schema validator](https://www.jsonschemavalidator.net/)


### Modeling
- [Web Sequence Diagrams](https://www.websequencediagrams.com/)
- [PlantUML](http://plantuml.com/)





## Docker

### Docker tutorials

- [Docker Curriculum - Docker for beginners](https://docker-curriculum.com/)
- [Docker Explained: Using Dockerfiles to Automate Building of Images](https://www.digitalocean.com/community/tutorials/docker-explained-using-dockerfiles-to-automate-building-of-images)
- [Docker Explained: How To Containerize Python Web Applications](https://www.digitalocean.com/community/tutorials/docker-explained-how-to-containerize-python-web-applications)
- [Docker Explained: How To Containerize and Use Nginx as a Proxy](https://www.digitalocean.com/community/tutorials/docker-explained-how-to-containerize-and-use-nginx-as-a-proxy)

### Docker Compose
- [Docker Compose ](https://docs.docker.com/compose/)
- [Docker Compose UI](https://github.com/francescou/docker-compose-ui)
- [Orchestrate Containers for Development with Docker Compose](https://blog.codeship.com/orchestrate-containers-for-development-with-docker-compose/)

### Docker - Best practices
- [@FuriKuri - Docker Best Practices](https://github.com/FuriKuri/docker-best-practices)


### Docker - Good to know
- [10 things to avoid in docker containers](https://developers.redhat.com/blog/2016/02/24/10-things-to-avoid-in-docker-containers/)
- [Java inside Docker](https://developers.redhat.com/blog/2017/03/14/java-inside-docker/)




## API management

### Kong - API Gateway
- [Kong - API Gateway ](https://getkong.org/)
- [Kongfig - Declarative configuration for Kong](https://github.com/mybuilder/kongfig)
- [Kong Dashboard - Dashboard for managing Kong gateway](https://github.com/PGBI/kong-dashboard)



## Libraries

### ReactJS
- [A collection of awesome things regarding React ecosystem](https://github.com/enaqx/awesome-react)
- [Boilerplate for Chrome Extension React.js project](https://github.com/jhen0409/react-chrome-extension-boilerplate)

#### React tutorials
- [Learning React With Create-React-App (Part 1)](https://medium.com/in-the-weeds/learning-react-with-create-react-app-part-1-a12e1833fdc)

#### React Problems

##### react-scripts start - ELIFECYCLE
- [npm start fails after copying a generated project to a new directory](https://github.com/facebook/create-react-app/issues/200)

### Python - Flask
- [Flask Framework](http://flask.pocoo.org/)
- [Deploy Python WSGI Applications Using uWSGI Web Server with Nginx](https://www.digitalocean.com/community/tutorials/how-to-deploy-python-wsgi-applications-using-uwsgi-web-server-with-nginx)
- [Flask-LogConfig ](http://flask-logconfig.readthedocs.io/en/latest/)

### JavaScript
- [hFigures - An interactive visualization library for health data with time points for continuous analysis](https://gitlab.com/ledancs/hFigures)

### Android
- [AWARE - Open-source Context Instrumentation Framework](http://www.awareframework.com/)

### Security
- [ABY - A Framework for Efficient Mixed-protocol Secure Two-party Computation](https://github.com/encryptogroup/ABY)
- [SCAPI - The Secure Computation API](https://github.com/cryptobiu/scapi)
- [FRESCO - a FRamework for Efficient and Secure COmputation](http://fresco.readthedocs.io/en/latest/index.html)
- []()
- []()
- []()
- []()
- []()
- []()
- []()

## Quality

### OpenTracing
- [opentracing.io](http://opentracing.io/)
- [OpenTracing Tutorial - Python](https://github.com/yurishkuro/opentracing-tutorial/tree/master/python)
- [Flask-OpenTracing](https://github.com/opentracing-contrib/python-flask)
- [Tutorial: Tracing Python Flask requests with OpenTracing](http://blog.scoutapp.com/articles/2018/01/15/tutorial-tracing-python-flask-requests-with-opentracing)


## Misc
- [GitLab - Team Handbook](https://about.gitlab.com/handbook/)
- [explainshell.com](https://explainshell.com/)


